import java.sql.*;

import java.util.Scanner;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;

public class Datenbank
{
	private static Connection c;
	private static Datenbank db;

	Datenbank() throws SQLException
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			c = DriverManager.getConnection("jdbc:mysql://localhost/Abfallbewirtschaftung", "root", "3080");
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	public static Datenbank getInstance() throws SQLException
	{
		if (db == null)
		{
			db = new Datenbank();
		}
		return db;
	}
	public void dao() throws SQLException
	{
		JdbcConnectionSource connectionSource =
				new JdbcConnectionSource("jdbc:h2:mem:fahrten");
		Dao<Muelltonnen, String> accountDao =
				DaoManager.createDao(connectionSource, Muelltonnen.class);

	}

	public void close()
	{
		try
		{
			c.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public static void main(String[] args) throws SQLException
	{
		getInstance();
	}
}