
public class Adresse
{
	public int AdresseID, PLZ, Hausnummer;
	public String Land, Strasse;
	public Adresse(int adresseID, int pLZ, int hausnummer, String land, String strasse) {
		super();
		AdresseID = adresseID;
		PLZ = pLZ;
		Hausnummer = hausnummer;
		Land = land;
		Strasse = strasse;
	}
	public int getAdresseID() {
		return AdresseID;
	}
	public void setAdresseID(int adresseID) {
		AdresseID = adresseID;
	}
	public int getPLZ() {
		return PLZ;
	}
	public void setPLZ(int pLZ) {
		PLZ = pLZ;
	}
	public int getHausnummer() {
		return Hausnummer;
	}
	public void setHausnummer(int hausnummer) {
		Hausnummer = hausnummer;
	}
	public String getLand() {
		return Land;
	}
	public void setLand(String land) {
		Land = land;
	}
	public String getStrasse() {
		return Strasse;
	}
	public void setStrasse(String strasse) {
		Strasse = strasse;
	}
	
}
