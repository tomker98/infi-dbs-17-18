import java.sql.*;

import java.util.Scanner;

public class Abfallbewirtschaftung {

	private static Connection c;
	private static Abfallbewirtschaftung db;

	Abfallbewirtschaftung() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			c = DriverManager.getConnection("jdbc:mysql://localhost/muellabfuhr", "root", "Iquoxum321");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Abfallbewirtschaftung getInstance() throws SQLException {
		if (db == null) {
			db = new Abfallbewirtschaftung();
		}
		return db;
	}

	public static void insertInto() {
		String sql = "";
		boolean err = false;
		PreparedStatement stmt = null;

		Scanner sc = new Scanner(System.in);

		while (true) {

			System.out.println("In welche Tabelle wollen Sie Dateneinfügen?:");
			String s = sc.next();

			do {
				switch (s) {
				case "Muelllaster":
					sql = "INSERT INTO Muelllaster (kennzeichen, Modellnummer) VALUES (?,?)";
					err = false;
					break;
				case "Muelltonnen":
					sql = "INSERT INTO Muelltonnen (Modellname, Fassungsvermoegen) VALUES (?,?)";
					err = false;
					break;
				case "Stadtbereich":
					sql = "INSERT INTO Stadtbereich (Name) VALUES (?)";
					err = false;
					break;
				case "HerstellerMuelllaster":
					sql = "INSERT INTO HerstellerMuelllaster (HerstellerName, Ansprechpartner, Adresse) VALUES (?,?,?)";
					err = false;
					break;
				case "HerstellerMuelltonnen":
					sql = "INSERT INTO HerstellerMuelltonnen (Name, Adresse, Ansprechperson) VALUES (?,?,?)";
					err = false;
					break;
				case "Fahrten":
					sql = "INSERT INTO Fahrten (Jahr, Monat, Tag) VALUES (?,?,?)";
					err = false;
					break;
				default:
					System.out.println("Falsche Eingabe");
					System.out.println("In welche Tabelle wollen Sie Dateneinfügen?:");
					s = sc.next();
					err = true;
				}
			} while (err);

			try {
				stmt = c.prepareStatement(sql);
				System.out.println("Bitte die Daten für " + s + " eingeben:");
				switch (s) {
				case "Muelllaster":
					System.out.println("Kennzeichen:");
					stmt.setString(1, sc.next());
					System.out.println("Modellnummer:");
					stmt.setInt(2, sc.nextInt());
					break;
				case "Muelltonnen":
					System.out.println("Modellname:");
					stmt.setString(1, sc.next());
					System.out.println("Fassungsvermögen:");
					stmt.setInt(2, sc.nextInt());
					break;
				case "HerstellerMuelllaster":
					System.out.println("Herstellername:");
					stmt.setString(1, sc.next());
					System.out.println("Ansprechpartner:");
					stmt.setString(2, sc.next());
					System.out.println("Adresse:");
					stmt.setString(3, sc.next());
					break;
				case "HerstellerMuelltonnen":
					System.out.println("Herstellername:");
					stmt.setString(1, sc.next());
					System.out.println("Adresse:");
					stmt.setString(2, sc.next());
					System.out.println("Ansprechperson:");
					stmt.setString(3, sc.next());
					break;
				case "Fahrten":
					System.out.println("Jahr:");
					stmt.setInt(1, sc.nextInt());
					System.out.println("Monat:");
					stmt.setString(2, sc.next());
					System.out.println("Tag:");
					stmt.setString(3, sc.next());
					break;
				case "Stadtbereich":
					System.out.println("Stadtbereichname:");
					stmt.setString(1, sc.next());
					break;
				}
				stmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void close() {
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws SQLException {
		getInstance();
		insertInto();

	}
}
