
public class LasterTonne {
	public int Tonne, Laster;
	public boolean Kompatibilitaet;
	public LasterTonne(int tonne, int laster, boolean kompatibilitaet) {
		super();
		Tonne = tonne;
		Laster = laster;
		Kompatibilitaet = kompatibilitaet;
	}
	public int getTonne() {
		return Tonne;
	}
	public void setTonne(int tonne) {
		Tonne = tonne;
	}
	public int getLaster() {
		return Laster;
	}
	public void setLaster(int laster) {
		Laster = laster;
	}
	public boolean isKompatibilitaet() {
		return Kompatibilitaet;
	}
	public void setKompatibilitaet(boolean kompatibilitaet) {
		Kompatibilitaet = kompatibilitaet;
	}
	
}
