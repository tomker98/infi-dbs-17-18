import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Muelltonnen")
public class Muelltonnen
{
	@DatabaseField(id = true)
	private int TonnenID;
	@DatabaseField
	private int TonnenStufe, Fassungsvermoegen, HerstellerID;
	private String Modellname;
	
	public Muelltonnen(int tonnenStufe, int tonnenID, int fassungsvermoegen, int herstellerID, String modellname)
	{
		super();
		TonnenStufe = tonnenStufe;
		TonnenID = tonnenID;
		Fassungsvermoegen = fassungsvermoegen;
		HerstellerID = herstellerID;
		Modellname = modellname;
	}
	public int getTonnenStufe() {
		return TonnenStufe;
	}
	public void setTonnenStufe(int tonnenStufe) {
		TonnenStufe = tonnenStufe;
	}
	public int getTonnenID() {
		return TonnenID;
	}
	public void setTonnenID(int tonnenID) {
		TonnenID = tonnenID;
	}
	public int getFassungsvermoegen() {
		return Fassungsvermoegen;
	}
	public void setFassungsvermoegen(int fassungsvermoegen) {
		Fassungsvermoegen = fassungsvermoegen;
	}
	public int getHerstellerID() {
		return HerstellerID;
	}
	public void setHerstellerID(int herstellerID) {
		HerstellerID = herstellerID;
	}
	public String getModellname() {
		return Modellname;
	}
	public void setModellname(String modellname) {
		Modellname = modellname;
	}
	
}
