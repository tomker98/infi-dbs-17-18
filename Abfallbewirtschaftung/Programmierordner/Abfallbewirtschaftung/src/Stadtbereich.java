
public class Stadtbereich {
	public String Name, Laster, Tag;

	public Stadtbereich(String name, String laster, String tag) {
		super();
		Name = name;
		Laster = laster;
		Tag = tag;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getLaster() {
		return Laster;
	}

	public void setLaster(String laster) {
		Laster = laster;
	}

	public String getTag() {
		return Tag;
	}

	public void setTag(String tag) {
		Tag = tag;
	}
	
}
