
public class Hersteller
{
	public int HerstellerID, AdresseID;
	public String Name, Ansprechpartner;
	public Hersteller(int herstellerID, int adresseID, String name, String ansprechpartner) {
		super();
		HerstellerID = herstellerID;
		AdresseID = adresseID;
		Name = name;
		Ansprechpartner = ansprechpartner;
	}
	public int getHerstellerID() {
		return HerstellerID;
	}
	public void setHerstellerID(int herstellerID) {
		HerstellerID = herstellerID;
	}
	public int getAdresseID() {
		return AdresseID;
	}
	public void setAdresseID(int adresseID) {
		AdresseID = adresseID;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getAnsprechpartner() {
		return Ansprechpartner;
	}
	public void setAnsprechpartner(String ansprechpartner) {
		Ansprechpartner = ansprechpartner;
	}
	
	
}
