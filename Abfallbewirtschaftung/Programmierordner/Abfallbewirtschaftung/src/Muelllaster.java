
public class Muelllaster
{
	public int TonnenStufe, Modellnummer, HerstellerID;
	public String Kennzeichen;
	public Muelllaster(int tonnenStufe, int modellnummer, int herstellerID, String kennzeichen) {
		super();
		TonnenStufe = tonnenStufe;
		Modellnummer = modellnummer;
		HerstellerID = herstellerID;
		Kennzeichen = kennzeichen;
	}
	public int getTonnenStufe() {
		return TonnenStufe;
	}
	public void setTonnenStufe(int tonnenStufe) {
		TonnenStufe = tonnenStufe;
	}
	public int getModellnummer() {
		return Modellnummer;
	}
	public void setModellnummer(int modellnummer) {
		Modellnummer = modellnummer;
	}
	public int getHerstellerID() {
		return HerstellerID;
	}
	public void setHerstellerID(int herstellerID) {
		HerstellerID = herstellerID;
	}
	public String getKennzeichen() {
		return Kennzeichen;
	}
	public void setKennzeichen(String kennzeichen) {
		Kennzeichen = kennzeichen;
	}
	
	
}
