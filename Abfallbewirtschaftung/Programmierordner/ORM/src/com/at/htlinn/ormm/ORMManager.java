package com.at.htlinn.ormm;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class ORMManager
{
	private final static String DATABASE_URL = "jdbc:sqlite:C:/Users/Thomas/Schule/2017-2018/INFI-DBS/dumptruck.db"; //sqlite3
	
	private Dao<DumpTruck, Integer> dumptruckDao;
	
	public static void main(String[] args) throws Exception
	{
		// turn our static method into an instance of Main
		new ORMManager().doMain(args);
	}
	
	private void doMain(String[] args) throws Exception
	{
		ConnectionSource connectionSource = null;
		try
		{
			// create our data-source for the database
			connectionSource = new JdbcConnectionSource(DATABASE_URL); //sqlite3
			//connectionSource = new JdbcConnectionSource("jdbc:mysql://localhost:3306/world", "root", "3080"); //mysql
			// setup our database and DAOs
			setupDatabase(connectionSource);
			// read and write some data
			readWriteData();
			// do a bunch of bulk operations
			System.out.println("\n\nIt seems to have worked\n\n");
		}
		finally
		{
			// destroy the data source which should close underlying connections
			if (connectionSource != null)
			{
				connectionSource.close();
			}
		}
	}

	/**
	 * Setup our database and DAOs
	 */
	private void setupDatabase(ConnectionSource connectionSource) throws Exception
	{
		dumptruckDao = DaoManager.createDao(connectionSource, DumpTruck.class);

		// if you need to create the table
		TableUtils.createTable(connectionSource, DumpTruck.class);
	}

	/**
	 * Read and write some example data.
	 */
	private void readWriteData() throws Exception
	{
		// create an instance of Account

		DumpTruck dumptruck1 = new DumpTruck("SZ-333L", "Volvo 1", 8000);

		// persist the account object to the database
		dumptruckDao.create(dumptruck1);
	}
}