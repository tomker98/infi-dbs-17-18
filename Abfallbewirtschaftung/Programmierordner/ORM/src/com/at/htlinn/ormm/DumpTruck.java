package com.at.htlinn.ormm;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "dumptrucks")
public class DumpTruck
{
	@DatabaseField(id = true)
	private String kennzeichen;
	@DatabaseField(canBeNull = false)
	private String lkwtyp;
	@DatabaseField(canBeNull = false)
	private int kapazitaet;
	
	public String getKennzeichen() {
		return kennzeichen;
	}
	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}
	public String getLkwtyp() {
		return lkwtyp;
	}
	public void setLkwtyp(String lkwtyp) {
		this.lkwtyp = lkwtyp;
	}
	public int getKapazitaet() {
		return kapazitaet;
	}
	public void setKapazitaet(int kapazitaet) {
		this.kapazitaet = kapazitaet;
	}
	public DumpTruck(){} //Default-Konstruktor (ben�tigt f�r ORMLite)
	public DumpTruck(String kennzeichen, String lkwtyp, int kapazitaet) {
		super();
		this.kennzeichen = kennzeichen;
		this.lkwtyp = lkwtyp;
		this.kapazitaet = kapazitaet;
	}

	public static void main(String[] args)
	{
		
	}
}