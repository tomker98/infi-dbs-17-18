import java.util.List;
import redis.clients.jedis.Jedis;

public class RedisJava
{
	public static void main(String[] args)
	{
		Jedis jedis = new Jedis("localhost"); 
		jedis.connect();
	    System.out.println("Connection to server sucessfully"); 
	    System.out.println("Server is running: "+jedis.ping());
	    jedis.del("Muelllaster", "Muelltonnen");
	    
	    //						Kennzeichen	  Fahrer	Bereich
	    jedis.lpush("Muellaster", "IL-1234F", "Eller", "Kranebitten");
	    jedis.lpush("Muellaster", "IL-1234R", "Kranebitter", "H�tting");
	    jedis.lpush("Muellaster", "IL-1234U", "Flatschi", "Reichenau");
	    
	    //							ID		Standort
	    jedis.lpush("Muelltonnen", "456", "Kranebitten");
	    jedis.lpush("Muelltonnen", "018", "Kranebitten");
	    jedis.lpush("Muelltonnen", "725", "Reichenau");
	    
	    List<String> listLaster = jedis.lrange("Muelllaster", 0 ,-1);
	    List<String> listTonne = jedis.lrange("Muelltonnen", 0 ,-1);
	    
	    for(int i = 0; i<listLaster.size(); i++)
	    { 
	    	System.out.println("M�lllaster: "+listLaster.get(i));
	    }
	    for(int i = 0; i<listTonne.size(); i++)
	    { 
	    	System.out.println("M�lltonne: "+listTonne.get(i));
	    } 
	    jedis.disconnect();
	}
}